public class SCN_CHTR_ChatterController {
    @AuraEnabled
    public static MyChatterUser getChatterUser() {
        User curUser = [SELECT FirstName, LastName, SmallPhotoUrl FROM User WHERE Id = :UserInfo.getUserId()];
        MyChatterUser chatterUser = new MyChatterUser(curUser);
        return chatterUser;
    }

    @AuraEnabled
    public static MyChatterUser getChatterUser(Id userId) {
        User curUser = [SELECT FirstName, LastName, SmallPhotoUrl FROM User WHERE Id = :userId];//:userInfo.getUserId()];
        MyChatterUser chatterUser = new MyChatterUser(curUser);
        return chatterUser;
    }

    @AuraEnabled
    public static List<MyFeedElement> getFeedElements(String recordId) {
        ConnectApi.FeedElementPage feedElementPage = getFeedElementsFromRecordFeed(recordId);
        return wrapMyFeedElements(feedElementPage);
    }

    private static ConnectApi.FeedElementPage getFeedElementsFromRecordFeed(String recordId) {
        ConnectApi.FeedElementPage page = ConnectApi.ChatterFeeds.getFeedElementsFromFeed(Network.getNetworkId(), ConnectApi.FeedType.Record, recordId, 25, ConnectApi.FeedDensity.AllUpdates, null, 100, ConnectApi.FeedSortOrder.CreatedDateDesc);
        return page;
    }

    private static List<MyFeedElement> wrapMyFeedElements(ConnectApi.FeedElementPage feedElementPage) {
        List<MyFeedElement> feedElements = new List<MyFeedElement>();
        if (feedElementPage == null) return feedElements;
        if (feedElementPage.elements.size() == 0) return feedElements;
        for (ConnectApi.FeedElement element : feedElementPage.elements) {
            MyFeedElement feedElement = new MyFeedElement(element);
            if (element.capabilities.comments.page.items.size() > 0) {
                for (ConnectApi.Comment feedComment : element.capabilities.comments.page.items) {
                    MyFeedElement comment = new MyFeedElement(feedComment);
                    feedElement.Comments.add(comment);
                }
            }
            feedElements.add(feedElement);
        }
        return feedElements;
    }

    private static String convertFeedBodyToHTML(ConnectApi.FeedBody body) {
        String convertedHTML = '';
        for (ConnectApi.MessageSegment segment : body.messageSegments) {
            if (segment instanceof ConnectApi.TextSegment) {
                ConnectApi.TextSegment textOutput = (ConnectApi.TextSegment) segment;
                convertedHTML += ' <span class="uiOutputText">' + textOutput.text + '</span>';
            }
            else if (segment instanceof ConnectApi.MentionSegment) {
                ConnectApi.MentionSegment mentionOutput = (ConnectApi.MentionSegment) segment;
                convertedHTML += ' <a class="cuf-entityLink cuf-mention" data-sfdc-wired-mouseover' +
                        ' data-sfdc-wired-mouseout data-sfdc-wired-focus data-sfdc-wired-blur' +
                        'href="#/sObject' + mentionOutput.record.Id + '/view"> <span class="uiOutputText">' + mentionOutput.text + '</span> </a>';
            }
            else if (segment instanceof ConnectApi.HashtagSegment) {
                ConnectApi.HashtagSegment hashtagOutput = (ConnectApi.HashtagSegment) segment;
                convertedHTML += ' <a class="cuf-entityLink cuf-hashtag" href="#/sObject' + hashtagOutput.tag + '/view">' +
                        ' <span class="uiOutputText">' + hashtagOutput.text + '</span> </a>';
            }
            else if (segment instanceof ConnectApi.LinkSegment) {
                ConnectApi.LinkSegment linkOutput = (ConnectApi.LinkSegment) segment;
                convertedHTML += ' <a href="' + linkOutput.url + '" title class="cuf-url forceOutputURL"' +
                        ' data-value="' + linkOutput.url + '">' + linkOutput.url + '</a>';
            }
            else if (segment instanceof ConnectApi.MarkupBeginSegment) {
                ConnectApi.MarkupBeginSegment markupBeginOutput = (ConnectApi.MarkupBeginSegment) segment;
                convertedHTML += ' <' + markupBeginOutput.htmlTag + '>';
            }
            else if (segment instanceof ConnectApi.MarkupEndSegment) {
                ConnectApi.MarkupEndSegment markupEndOutput = (ConnectApi.MarkupEndSegment) segment;
                convertedHTML += ' </' + markupEndOutput.htmlTag + '>';
            }
            else if (segment instanceof ConnectApi.InlineImageSegment) {
                ConnectApi.InlineImageSegment inlineImageOutput = (ConnectApi.InlineImageSegment) segment;
                convertedHTML += ' <img alt="' + inlineImageOutput.altText + '" title="' + inlineImageOutput.altText +
                        '" data-fileid="' + inlineImageOutput.thumbnails.fileId +
                        '" style="height: auto; max-width: 100%; max-height: 360px;"' +
                        ' class="slds-text-link--reset" src="' + inlineImageOutput.thumbnails.previews[0].previewUrls[0].previewURL + '"/>';
            }
            else {
                // The other segment types are system-generated and have no corresponding input types.
            }

        }
        return convertedHTML;
    }

    public class MyChatterUser {
        @AuraEnabled
        public String UserId { get; set; }
        @AuraEnabled
        public String UserName { get; set; }
        @AuraEnabled
        public String UserPhotoURL { get; set; }

        public MyChatterUser(User usr) {
            this.UserId = String.valueOf(usr.Id);
            this.UserName = usr.FirstName + ' ' + usr.LastName;
            this.UserPhotoURL = usr.SmallPhotoUrl;
        }
    }

    public class MyFeedElement {
        @AuraEnabled
        public Id Id { get; set; }
        @AuraEnabled
        public MyChatterUser ChatterUser { get; set; }
        @AuraEnabled
        public Datetime CreatedDate { get; set; }
        @AuraEnabled
        public Datetime LastModifiedDate { get; set; }
        @AuraEnabled
        public Integer LikeCount { get; set; }
        @AuraEnabled
        public String Content { get; set; }
        @AuraEnabled
        public List<MyFeedElement> Comments { get; set; }
        //add by huang 2018/5/14
        @AuraEnabled
        public Boolean isLiked { get; set; }    


        public MyFeedElement(ConnectApi.FeedElement feedElement) {
            this.Id = feedElement.Id;
            this.ChatterUser = getChatterUser(feedElement.parent.Id);
            this.CreatedDate = Datetime.valueOf(feedElement.createdDate);
            this.LastModifiedDate = Datetime.valueOf(feedElement.modifiedDate);
            this.LikeCount = feedElement.capabilities.chatterLikes.page.total;
            this.Content = convertFeedBodyToHTML(feedElement.body);//feedElement.body.text;
            this.isLiked = feedElement.capabilities.chatterLikes.myLike<>null?true:false;
            this.Comments = new List<MyFeedElement>();
        }

        public MyFeedElement(ConnectApi.Comment feedComment) {
            this.Id = feedComment.Id;
            this.ChatterUser = getChatterUser(feedComment.user.Id);
            this.CreatedDate = Datetime.valueOf(feedComment.createdDate);
            this.LastModifiedDate = Datetime.valueOf(feedComment.capabilities.edit.lastEditedDate);
            this.LikeCount = feedComment.likes.total;
            this.Content = convertFeedBodyToHTML(feedComment.body);//feedComment.body.text;
            this.isLiked = feedComment.myLike<>null?true:false;
            this.Comments = new List<MyFeedElement>();
        }
    }
}