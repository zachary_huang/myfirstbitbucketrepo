public class ProjectCalloutService {
    //Complete the implementation for business and callout logic
	@InvocableMethod(label='Post Opportunity To PMS' description='invokes a callout to asynchronousially update a PMS record.')
	public static void postOpportunityToPMS(List<Id> pids){
       	 QueueablePMSCall qpc = new QueueablePMSCall(pids);	
       	 ID jobID = System.enqueueJob(qpc);	
    }


    // inner class for executing real logic
    public class QueueablePMSCall implements system.Queueable, Database.AllowsCallouts {
    	Id oppId;
    	String token;

    	public QueueablePMSCall(List<Id> oppIds) {
            this.oppId = oppIds[0];
            this.token = ServiceTokens__c.getValues('ProjectServiceToken').Token__c;
        }

        public void execute(QueueableContext Context){
        	Opportunity opp = [SELECT Id, Name, Account.Name, CloseDate, Amount 
                               FROM Opportunity 
                               WHERE Id = :oppId];

             String closeDate = String.valueOf(opp.CloseDate.year()) + '-' +
                String.valueOf(opp.CloseDate.month()).leftPad(2,'0') + '-' +
                String.valueOf(opp.CloseDate.day()).leftPad(2,'0'); 

             System.JSONGenerator gen = JSON.createGenerator(true);
             gen.writeStartObject();
             gen.writeObjectField('opportunityId', opp.Id);
             gen.writeObjectField('opportunityName', opp.Name);
             gen.writeObjectField('accountName', opp.account.Name);
             gen.writeObjectField('closeDate', opp.CloseDate);
             gen.writeObjectField('amount', opp.Amount);
             gen.writeEndObject();

             String jsonBody = gen.getAsString();

             calloutTOPMS(opp.Id,jsonBody,token);


        }
    }

    @future(callout = true)
    public static void calloutTOPMS(Id poId, String jsonBody,String token){
  
    	Opportunity opp = new Opportunity(Id = poId);
    	Http htp = new Http();
    	HttpRequest request = new HttpRequest();
    	
    	request.setEndpoint('callout:ProjectService');
    	request.setMethod('POST');
    	request.setTimeout(120000);
    	request.setHeader('Content-Type', 'application/JSON;charset=UTF-8');
    	request.setHeader('token', token);
    	request.setBody(jsonBody);
    	HttpResponse  response = htp.send(request);
        System.debug(LoggingLevel.INFO, '*** response: ' + response.getStatus());
    	if(response.getStatus() == 'OK'){
    		// success
    		opp.StageName = 'Submitted Project';
    	 	
    	}else{
    		opp.StageName = 'Resubmit Project';
    		 System.debug('The status code returned was not expected: ' +
                         response.getStatusCode() + ' ' + response.getStatus());
    	}

    	update opp;

        System.debug(LoggingLevel.INFO, '*** finishUpdate: ');
    }
}