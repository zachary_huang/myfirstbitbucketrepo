@isTest
private class ProjectCalloutServiceTest {
    static Account acc;
    static Opportunity opp1;

    @testSetup
    private static void setupData(){
        ServiceTokens__c newServiceToken = new ServiceTokens__c();
        newServiceToken.Name = 'ProjectServiceToken';
        newServiceToken.Token__c = 'TestingPurpose';
        insert newServiceToken;

        

    }
    
    @isTest
    public static void testProjectCalloutFailure() {
        acc = new Account();
        acc.Name = 'accTest';
        insert acc;

        opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.CloseDate = System.today().addDays(3);
        opp1.AccountId = acc.Id;
        opp1.Amount = 1000;
        opp1.StageName = 'Prospecting';
        
        insert opp1;

        Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMockFailure());

        Test.startTest();
        System.debug(LoggingLevel.INFO, '*** start: ' );
        ProjectCalloutService.postOpportunityToPMS(new List<Id>{opp1.Id});
        Test.stopTest();
        System.debug(LoggingLevel.INFO, '*** failure: ' );
        Opportunity result2 = [SELECT Id,StageName FROM Opportunity limit 1];
        System.assertEquals('Prospecting', result2.StageName);

    }

    @isTest
    public static void testProjectCallout() {
        acc = new Account();
        acc.Name = 'accTest';
        insert acc;

        opp1 = new Opportunity();
        opp1.Name = 'testOpp1';
        opp1.CloseDate = System.today().addDays(3);
        opp1.AccountId = acc.Id;
        opp1.Amount = 1000;
        opp1.StageName = 'Prospecting';
        
        insert opp1;

        Test.setMock(HttpCalloutMock.class, new ProjectCalloutServiceMock()); 
        Test.startTest();
        ProjectCalloutService.postOpportunityToPMS(new List<Id>{opp1.Id});
        Test.stopTest();
        Opportunity result = [SELECT Id,StageName FROM Opportunity where Name='testOpp1'];
        System.assertEquals('Prospecting', result.StageName);

    }
}