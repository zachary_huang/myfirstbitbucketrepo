public with sharing class CampingListController {
	@AuraEnabled
    public static List<Camping_Item__c> getItems(){
		return [Select id,Name,Packed__c,Price__c,Quantity__c from Camping_Item__c];
    }
	@AuraEnabled
    public static Camping_Item__c saveItem(Camping_Item__c camp){
        upsert camp;
        return camp;
	}
}