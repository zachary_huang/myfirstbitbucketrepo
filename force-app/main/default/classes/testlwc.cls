public with sharing class testlwc {
    public static Map<String, Decimal> mapDataMap= new Map<String, Decimal>{
        '直接访问' => 335,
        '邮件营销' => 310,
        '联盟广告' => 234,
        '视频广告' => 135,
        '搜索引擎' => 1548
    }; 
    
    public class TestWrapper{
        @AuraEnabled public String total{set;get;} 
        @AuraEnabled public List<DataInfo> lstData{set;get;}
    }
    public class DataInfo{
        @AuraEnabled public String label{set;get;}
        @AuraEnabled public Decimal value{set;get;}
    }

    @AuraEnabled
    public static TestWrapper getDataInfo(String recordId){
        TestWrapper objWrapper;
        List<DataInfo> lstData = new List<DataInfo>();
        if(recordId == null){
            return objWrapper;
        }

        Decimal total=0;
        for(String strkey : mapDataMap.keyset()){
            DataInfo objData = new DataInfo();
            objData.label = strkey;
            objData.value = mapDataMap.get(strkey);
            total += objData.value;
            lstData.add(objData);
        }

        objWrapper = new TestWrapper();
        objWrapper.total = String.valueOf(total);
        objWrapper.lstData = lstData;
        return objWrapper;
    }
}