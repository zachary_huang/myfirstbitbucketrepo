@isTest
private class MaintenanceRequestHelperTest {
    static testMethod void testMethod1() {
    	List<case> cList = new List<Case>();
	    for(Integer i=0;i<300;i++){
			case c = new case(
	        	Type='Routine Maintenance',
	        	status='New',
	        	subject='Test'
	        	);
			clist.add(c);

	    }
	    insert clist;

	    List<Product2> proList=  new List<Product2>();
	    for(Integer i=0;i<7;i++){
	    	Product2 p = new Product2(
        		name='pro'+i,
        		Replacement_Part__c=true,
        		Maintenance_Cycle__c = i
        	);	
        	proList.add(p);
	    }
        
        insert proList;
        
        List<Work_Part__c> wpList = new List<Work_Part__c>();
        for(Integer i=0;i<600;i++){
        	Work_Part__c  wp = new Work_Part__c(
        		Maintenance_Request__c=clist[math.mod(i,300)].id,
        		Quantity__c = 10,
        		Equipment__c = prolist[math.mod(i,7)].id
        	);
        	wplist.add(wp);
        }
        insert wplist;

      	for(case c:clist){
      		c.status = 'Closed';
      	}
      	system.debug('size:'+clist.size());
      	Test.startTest();
        update clist;
        test.stopTest();
        system.assert(300==[select count() from case where status ='New']);
        
    }

    static testMethod void testMethod2() {


        case c = new case(
        	Type='Routine Maintenance',
        	status='New',
        	subject='Test'
        	);

        insert c;

        c.status = 'Closed';
        update c;

        case cnew = [select id,Date_Reported__c,subject from case  where status ='New' and Type ='Routine Maintenance'];
        System.assert(cnew.Date_Reported__c == date.today());
        system.assert(cnew.subject == 'Test');
    }
}