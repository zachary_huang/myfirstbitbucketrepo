public with sharing class ContactController {
	public Integer sumRecords{get;set;}
	public Integer pagesize{get;set;}
	
	// instantiate StandardSetController 
    public ApexPages.StandardSetController con{
    	get{
    		system.debug('init con');
    		if(con == null){
    			con = new ApexPages.StandardSetController(Database.getQueryLocator([Select id,name,email,phone from contact limit 200]));
    			sumRecords = [Select id,name,email,phone from contact limit 200].size();
    			if(pagesize ==null){
    				pagesize=5;
    				system.debug('first init pagesize:'+pagesize+'==first init sumRecords:'+sumRecords);
    			}
    			con.setPageSize(pagesize);
    		}
    		return con;
    	}
    	set;

    }


    public ContactController(){

    }

    public Id reportId{
    	get{
    		return [select id from report limit 1][0].Id;
    	}
    }

    //put records in a list
    public List<Contact> contactList{
    	get{
    		system.debug('init contactList');
    		if(con != null){
    			return (list<contact>)con.getRecords(); 
    		}else{
    			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'未获取相关记录！'));
    			return null;
    		}

    	}
    	set;
    }

    //updatePage
    public void updatePage(){
    	system.debug('pagesize:'+pagesize);
    	con.setPageSize(pagesize);
    	contactList=(list<contact>)con.getRecords();

    }
    //current pageNumber
    public Integer pageNum{
    	get{
    		if(con <> null){
    			return con.getPageNumber();
    		}else{
    			return null;
    		}
    	}
    	set{
    		//pageNum = Integer.valueOf(ApexPages.currentPage().getParameters().get('pageNum'));
    	}
    }

    //total pageNumber
    public Integer totalNumber{
    	get{
    		if(con <> null){
    			Integer sum = Math.mod(sumRecords, pagesize )>0?sumRecords/pagesize + 1:sumRecords/pagesize;
    			system.debug('sum:'+sum+'sumRecords:'+sumRecords+'pagesize:'+pagesize);
    			return sum;
    		}else{
    			return null;
    		}
    	}
    	set;
    }

    // //go to the definite page
  	public void gotoPage(Integer pagenum){
  		con.setpageNumber(pagenum);
  	}

    //define the  first,last,next,previous
    public Boolean hasNext(){
    	return con.getHasNext();
    }

    public Boolean hasPrevious(){
    	return con.getHasPrevious();
    }

    public void goFirst(){
    	 con.first();
    }

    public void goLast(){
    	 con.last();
    }

    public void goNext(){
    	 con.next();
    }

    public void goPrevious(){
    	 con.previous();
    }

    public PageReference  cancel(){
    	return con.cancel();
    }

}