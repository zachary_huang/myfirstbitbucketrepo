public class TestCtrl {
    public static String value{set;get;}
    @AuraEnabled
    public static void setName(String name) {
        value = name;
    }
    @AuraEnabled
    public static String getName(){
        return value;
    }
}