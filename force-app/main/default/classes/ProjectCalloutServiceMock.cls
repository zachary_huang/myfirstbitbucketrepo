public class ProjectCalloutServiceMock implements  HttpCalloutMock {
   //Implement http mock callout here
    public HTTPResponse respond(HTTPRequest request) {
        // Create a fake response
        HttpResponse response = new HttpResponse();
        response.setHeader('Content-Type', 'application/json');
        response.setBody('success');
        response.setStatusCode(200);
        response.setStatus('OK');
        return response; 
    }
}