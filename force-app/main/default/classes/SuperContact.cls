public class SuperContact {
    @future(callout=true)
    public static void uploadToS3(String attId){
        Attachment attach = [Select id,Body,
                                            ContentType,
                                            Name, parentId from Attachment where id =:attId];
        Id conId = attach.ParentId;
		String attachmentBody = EncodingUtil.base64Encode(attach.Body);
        String formattedDateString = Datetime.now().formatGMT('EEE, dd MMM yyyy HH:mm:ss z');
        String key = 'AKIAJIM237ADZEDJXBPA';
        String secret = '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';
        String bucketname = 'zarchary-bucket';
        String host = 's3.amazonaws.com';
        String method = 'PUT';
        String filename = attach.Id + '-' + attach.Name;
        system.debug('attach.name:'+attach.Name);
        
        HttpRequest req = new HttpRequest();
        req.setMethod(method);
        req.setEndpoint('https://' + bucketname + '.' + host + '/' + bucketname + '/' + filename );
        req.setHeader('Host', bucketname + '.' + host);
        req.setHeader('Content-Length', String.valueOf(attachmentBody.length()));
        req.setHeader('Content-Encoding', 'UTF-8');
        req.setHeader('Content-type', attach.ContentType);
        req.setHeader('Connection', 'keep-alive');
        req.setHeader('Date', formattedDateString);
        req.setHeader('ACL', 'public-read-write');
        //req.setBody(attachmentBody);
        Blob blobBody = EncodingUtil.base64Decode(attachmentBody);
        req.setBodyAsBlob(blobBody);
        
        String stringToSign = method + '\n\n' +
            attach.ContentType + '\n' +
            formattedDateString+ '\n' +
            '/' + bucketname + '/' + bucketname + '/' + filename;
        
        system.debug('stringToSign:'+stringToSign);
        String encodedStringToSign = EncodingUtil.urlEncode(stringToSign, 'UTF-8');
        Blob mac = Crypto.generateMac('HMACSHA1', blob.valueof(stringToSign),blob.valueof(secret));
        String signed = EncodingUtil.base64Encode(mac);
        String authHeader = 'AWS' + ' ' + key + ':' + signed;
        System.debug('authHeader:'+authHeader);
        req.setHeader('Authorization',authHeader);
        String decoded = EncodingUtil.urlDecode(encodedStringToSign , 'UTF-8');
        
        Http http = new Http();
        HTTPResponse res = http.send(req);
        System.debug('*Resp:' + String.ValueOF(res.getBody()));
        System.debug('RESPONSE STRING: ' + res.toString());
        System.debug('RESPONSE STATUS: ' + res.getStatus());
        System.debug('STATUS_CODE: ' + res.getStatusCode());
        if(res.getStatusCode()== 200){
			Contact con = [Select id,S3ImageUrl__c from Contact where id =:conId];
            con.S3ImageUrl__c = 'https://' + bucketname + '.' + host + '/' + bucketname + '/' + filename;
            update con;
        }
    }
    
}