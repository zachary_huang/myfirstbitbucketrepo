public class Product2Extension {

    public List<ProductWrapper> productsToInsert {get;set;}

    public Product2Extension(ApexPages.StandardController controller){
        productsToInsert = new List<ProductWrapper>();
        addRows();
    }

    public void AddRows(){
        Integer defaultRows = Constants.DEFAULT_ROWS;
        for ( Integer i=0; i<defaultRows; i++ ){
            productsToInsert.add( new productWrapper() );
        }
    }

    public List<ChartHelper.ChartData> GetInventory(){
        return ChartHelper.GetInventory();
    }

    public PageReference Save(){
        Savepoint sp = Database.setSavepoint();
        try {
            Map<Integer, Product2> productMap = new Map<Integer, Product2>();
            Map<Integer, PriceBookEntry> priceBookMap = new Map<Integer, PriceBookEntry>();
            Integer index = 0;
            for(ProductWrapper pw :  productsToInsert){
                if(String.isNotBlank(pw.productRecord.Name) && pw.productRecord.Family!=null
                    && pw.productRecord.isActive!=null && pw.pricebookEntryRecord.UnitPrice!=null && pw.pricebookEntryRecord.UnitPrice!=0 && pw.productRecord.Initial_Inventory__c!=null){

                    productMap.put(index, pw.productRecord);
                    priceBookMap.put(index, pw.pricebookEntryRecord);
                    index++;
                }
            }
            insert productMap.values();


            List<PriceBookEntry> pbList = new List<PriceBookEntry>();
            for(Integer pbIndex : productMap.keySet()){
                if(priceBookMap.containsKey(pbIndex) && productMap.get(pbIndex).Id!=null){
                    PriceBookEntry currentPBEntry = priceBookMap.get(pbIndex);
                    currentPBEntry.Pricebook2Id = Constants.STANDARD_PRICEBOOK_ID;
                    currentPBEntry.product2Id = productMap.get(pbIndex).Id;
                    currentPBEntry.isActive = true;
                    pbList.add(currentPBEntry);
                }
            }
            insert pbList;

            //If successful clear the list and display an informational message
            apexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO,productsToInsert.size()+' Inserted'));
            productsToInsert.clear();   //Do not remove
            addRows();  //Do not remove
        } catch (Exception e){
            apexPages.addMessage(new ApexPages.message(ApexPages.Severity.INFO,Constants.ERROR_MESSAGE));
            Database.rollback(sp);
        }
        return null;
    }


    public List<SelectOption> GetFamilyOptions(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption(Constants.SELECT_ONE, Constants.SELECT_ONE));
        for(Schema.PicklistEntry entry : Constants.PRODUCT_FAMILY){
            options.add(new SelectOption(entry.getValue(), entry.getLabel()));
        }

        return options;
    }


    public class ProductWrapper{
       public Product2 productRecord {set;get;}
       public PriceBookEntry pricebookEntryRecord {set;get;}

       public ProductWrapper() {
            productRecord = new Product2(Initial_Inventory__c =0);
            pricebookEntryRecord = new PricebookEntry(Unitprice=0.0);
        }
    }


}