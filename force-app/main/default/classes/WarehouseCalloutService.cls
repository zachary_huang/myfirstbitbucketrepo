public with sharing class WarehouseCalloutService {

    private static final String WAREHOUSE_URL = 'https://th-superbadge-apex.herokuapp.com/equipment';
    
    // complete this method to make the callout (using @future) to the
    // REST endpoint and update equipment on hand.
    @future(callout=true)
    public static void runWarehouseEquipmentSync(){
        http http = new http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(WAREHOUSE_URL);
        request.setMethod('GET');
        HttpResponse response = http.send(request);

        if(response.getStatusCode() == 200){
        	List<Object> results = (List<object>) JSON.deserializeUntyped(response.getBody());
        	system.debug('results:'+(Map<String,object>)results[0]);
        	upsertWH(results);
        }
    }

    private static void upsertWH(List<object> results){
    	List<Product2> proList = new List<Product2>();
    	Map<string,Object> wcMap = new Map<String,Object>();
    	for(Object wc: results){
    		wcMap = (Map<String,object>)wc;
    		Product2 p = new Product2();
    		p.Replacement_Part__c = true;
    		p.Cost__c = (Decimal)wcMap.get('cost');
    		p.Current_Inventory__c = (Decimal)wcMap.get('quantity');
    		p.Warehouse_SKU__c = (String)wcMap.get('sku');
    		p.Lifespan_Months__c = (Decimal)wcMap.get('lifespan');
    		p.Maintenance_Cycle__c = (Decimal)wcMap.get('maintenanceperiod');
    		p.Name = (String)wcMap.get('name');
    		p.ProductCode = (String)wcMap.get('_id');

    		proList.add(p);
    	}
    	if(proList.size()>0){

    		upsert proList Product2.fields.Warehouse_SKU__c;
    	}
    }
}