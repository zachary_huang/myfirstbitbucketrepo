/**
 * @name TestDataFactory
 * @description Contains methods to construct and/or validate commonly used records
**/
public with sharing class TestDataFactory {

    /**
     * @name ConstructCollaborationGroup
     * @description
    **/
    
    public static CollaborationGroup ConstructCollaborationGroup(){
        //ToDo: Ensure this method returns a single Chatter CollaborationGroup
        //    whose Name starts with 'TEST' followed by the INVENTORY_ANNOUNCEMENTS constant
        //    and configured so anyone can join, see and post updates.
        CollaborationGroup chatterGroup = new CollaborationGroup(
            Name = 'TEST' + Constants.INVENTORY_ANNOUNCEMENTS,
            CollaborationType = 'Public'
        );
        return chatterGroup;
    }

    /**
     * @name CreateProducts
     * @description Constructs a list of Product2 records for unit tests
    **/
    public static List<Product2> ConstructProducts(Integer cnt){
        List<Product2> prolst = new List<Product2>();
        //ToDo: Ensure this method returns a list, of size cnt, of uniquely named Product2 records
        //  with all the required fields populated
        //  and IsActive = true
        //  an Initial Inventory set to 10
        //  and iterating through the product family picklist values throughout the list.

        for(Integer i=1; i<=cnt; i++){
            Product2 pro = new Product2();
            pro.IsActive = true;
            pro.Name = 'test product' + i;
            pro.Initial_Inventory__c = 10;
            pro.Family = Constants.PRODUCT_FAMILY.get(math.mod(i,4)).getValue();
            prolst.add(pro);
        }

        return prolst;
    }

    /**
     * @name CreatePricebookEntries
     * @description Constructs a list of PricebookEntry records for unit tests
    **/
    public static List<PricebookEntry> ConstructPricebookEntries(List<Product2> prods){
        List<PricebookEntry> priceEntrylst = new List<PricebookEntry>();
        //ToDo: Ensure this method returns a corresponding list of PricebookEntries records
        //  related to the provided Products
        //  with all the required fields populated
        //  and IsActive = true
        //  and belonging to the standard Pricebook
        for(Product2 pro : prods){
            PricebookEntry priceEntry = new PricebookEntry();
            priceEntry.IsActive = true;
            priceEntry.Pricebook2Id = Constants.STANDARD_PRICEBOOK_ID;
            priceEntry.product2Id = pro.Id;
            priceEntry.UnitPrice = 100;
            priceEntrylst.add(priceEntry);
        }

        return priceEntrylst;
    }

    /**
     * @name CreateAccounts
     * @description Constructs a list of Account records for unit tests
    **/
    public static List<Account> ConstructAccounts(Integer cnt){
        List<Account> acclst = new List<Account>();
        //ToDo: Ensure this method returns a list of size cnt of uniquely named Account records
        //  with all of the required fields populated.
        for(Integer i=0; i<cnt; i++){
            Account acc = new Account(
                Name = 'Test Account' + i
            );
            acclst.add(acc);
        }
        return acclst;
    }

    /**
     * @name CreateContacts
     * @description Constructs a list of Contacxt records for unit tests
    **/
    public static List<Contact> ConstructContacts(Integer cnt, List<Account> accts){
        List<Contact> conlst = new List<Contact>();
        //ToDo: Ensure this method returns a list, of size cnt, of uniquely named Contact records
        //  related to the provided Accounts
        //  with all of the required fields populated.
        for(Integer i=0; i<cnt; i++ ){
            Contact con = new Contact();
            Integer index = Math.mod(i, accts.size());
            con.LastName = 'Test Contact' + i;
            con.AccountId = accts.get(index).Id;
            conlst.add(con);
        }
        return conlst;
    }

    /**
     * @name CreateOrders
     * @description Constructs a list of Order records for unit tests
    **/
    public static List<Order> ConstructOrders(Integer cnt, List<Account> accts){
        List<Order> orderlst = new List<Order>();
        //ToDo: Ensure this method returns a list of size cnt of uniquely named Order records
        //  related to the provided Accounts
        //  with all of the required fields populated.
        for(Integer i=0; i<cnt; i++){
            Integer index = Math.mod(i, accts.size());
            Order o = new Order();
            o.AccountId = accts.get(index).Id;
            o.Pricebook2Id = Constants.STANDARD_PRICEBOOK_ID;
            o.Status = 'Draft';
            o.EffectiveDate = System.today();
            orderlst.add(o);
        }
        return orderlst;
    }

    /**
     * @name CreateOrderItems
     * @description Constructs a list of OrderItem records for unit tests
    **/
    public static List<OrderItem> ConstructOrderItems(integer cnt, list<pricebookentry> pbes, list<order> ords){
        
        //ToDo: Ensure this method returns a list of size cnt of OrderItem records
        //  related to the provided Pricebook Entries
        //  and related to the provided Orders
        //  with all of the required fields populated.
        //  Hint: Use the DEFAULT_ROWS constant for Quantity as it will be used in the next challenge
        List<OrderItem> orderItemlst = new List<OrderItem>();
        for(Integer i = 0; i <cnt; i++) {
            OrderItem ord = new OrderItem();
            ord.PricebookEntryId = pbes.get(math.mod(i, pbes.size())).Id;
            ord.OrderId = ords.get(math.mod(i, ords.size())).Id;
            ord.Quantity = Constants.DEFAULT_ROWS;
            ord.UnitPrice = 250;
            orderItemlst.add(ord);
        }
        
        return orderItemlst;
    }

    /**
     * @name SetupTestData
     * @description Inserts accounts, contacts, Products, PricebookEntries, Orders, and OrderItems.
    **/
    public static void InsertTestData(Integer cnt){
        //ToDo: Ensure this method calls each of the construct methods
        //  and inserts the results for use as test data.

        CollaborationGroup groups = TestDataFactory.ConstructCollaborationGroup();
        insert groups;
        
        List<Product2>  products= TestDataFactory.ConstructProducts(cnt);
        insert products;
        
        List<PriceBookEntry> entries = TestDataFactory.ConstructPricebookEntries(products);
        insert entries;
        
        List<Account> accts = TestDataFactory.ConstructAccounts(cnt);
        insert accts;
        
        List<Contact> contacts = TestDataFactory.ConstructContacts(cnt,accts);
        insert contacts;
        
        List<Order> orders = TestDataFactory.ConstructOrders( cnt,  accts);
        insert orders;
        
        List<OrderItem> items = TestDataFactory.ConstructOrderItems(cnt, entries, orders);
        insert items;

    }

    public static void VerifyQuantityOrdered(Product2 originalProduct, Product2 updatedProduct, Integer qtyOrdered) {
        System.assertEquals((updatedProduct.Quantity_Ordered__c - originalProduct.Quantity_Ordered__c), qtyOrdered);
    }

}