@isTest
private class ProjectRESTServiceTest {
  //Implement Apex REST service tests here
    @isTest
    private static void  postProjectDataTest(){
        Account acc = new Account();
        acc.Name = 'testAcc';
        insert acc;

        Opportunity opp = new Opportunity();
        opp.Name = 'testOpp';
        opp.AccountId = acc.Id;
        opp.StageName = 'Prospecting';
        opp.CloseDate = System.today();
        opp.DeliveryInstallationStatus__c = 'Yet to begin';
        insert opp;

        Project__c project = new Project__c();
        project.ProjectRef__c = 'testProjectRef';
        project.Opportunity__c = opp.Id;
        project.Status__c = 'Running';
        insert project;

        Date StartDate = System.today();
        Date EndDate = System.today().addDays(30);

        Test.startTest();
        Opportunity opp1 = [select id from Opportunity];
        System.debug(LoggingLevel.INFO, '*** opp1.Id: ' + opp1.Id);
        String res = ProjectRESTService.postProjectData('testProjectRef', 'testProjectName',opp1.Id , StartDate, EndDate, 1000, 'Running');
        Test.stopTest();
        System.assertEquals('OK', res);

        Opportunity targetOpp = [Select Id, DeliveryInstallationStatus__c FROM Opportunity WHERE Id =: opp.Id];
        // System.assertEquals('testProjectRef', targetOpp.DeliveryInstallationStatus__c);

        String res2 = ProjectRESTService.postProjectData('testProjectRef', 'testProjectName','test' , StartDate, EndDate, 1000, '123test');
        System.assertNotEquals('OK', res2);
        
    }

}