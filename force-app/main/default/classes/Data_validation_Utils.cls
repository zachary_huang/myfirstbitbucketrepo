/****************************************************
* Name: Data_validation_Utils.cls
* Author: Zachary Huang
* CreateDate: 2018-5-28
* Description: Methods for data validation
****************************************************/
public with sharing class Data_validation_Utils {
    
    /**
    * formatType:
    * '1' stand for ','
    * '2' stand for ';'
    * '3' stand for ',&;'
    * '4' stand for ';&,'
    * String for field name  List for type
    *[list[0] for formatType, list[1] for hasId(0 for false/1 for true),list[2] for sublist.length,list[3] for index of ID]
    **/
	// public Static Map<String,List<Integer>> dataValidFormatMap = new Map<String,List<Integer>>{
	// 	'USA_CHATTER_GROUP_TEAM_MEMBERSHIP__C' => new List<Integer>{3,1,2,2},
	// 	'USA_ASSIGNED_MARKETS__C' =>  new List<Integer>{4,0,2,0},
	// 	'USA_MARKET__C' =>  new List<Integer>{1,0,1,2},
	// 	'USA_NATIONAL_ID__C' => new List<Integer>{1,0,1,0},
	// 	'USA_SUB_TIER__C' => new List<Integer>{1,0,1,0},
	// 	'USA_TD_LINX_ID__C' => new List<Integer>{1,0,1,0},
	// 	'USA_TIER__C' => new List<Integer>{1,0,1,0},
	// 	'USA_POSSIBLE_VALUES__C' => new List<Integer>{2,0,1,0},
	// 	'MAP_SUBTIER_FILTERS__C' => new List<Integer>{1,0,1,0},
	// 	'MAP_TIER_FILTERS__C' => new List<Integer>{1,0,1,0},
	// 	'MAP_WINES_SUBTIER_FILTERS__C' => new List<Integer>{1,0,1,0},
	// 	'MAP_WINES_TIER_FILTERS__C' => new List<Integer>{1,0,1,0},
	// 	'GLOBAL_CHATTER_GROUPS__C' => new List<Integer>{1,1,1,1},
	// 	'MAP_REP_FILTERS__C' => new List<Integer>{4,1,2,2},
	// 	'GLOBAL_REPORT_IDS__C' => new List<Integer>{1,1,1,1},
	// 	'REPORT_IDS__C' => new List<Integer>{1,1,1,1},
	// 	'TO_DO_CATEGORIES__C' => new List<Integer>{1,0,1,0},
	// 	'CHAIN_NAMES__C' => new List<Integer>{1,0,1,0},
	// 	'MANAGERS__C' => new List<Integer>{4,1,2,2},
	// 	'REPORT_NAMES__C' => new List<Integer>{1,0,1,0}
	// };


	public static List<String> statusList = new List<String>{
		'error','fixed','good'
	};


	//field name => IdS sample
	public Static Map<String,String> dataIDCheckMap = new Map<String,String>{
		'USA_CHATTER_GROUP_TEAM_MEMBERSHIP__C' => '0F9Dxxxxxxxxxxx',
		'GLOBAL_CHATTER_GROUPS__C' => '0F9Dxxxxxxxxxxx',
		'MAP_REP_FILTERS__C' => '005Dxxxxxxxxxxxxxx',
		'GLOBAL_REPORT_IDS__C' => '00ODxxxxxxxxxxx',
		'REPORT_IDS__C' => '00ODxxxxxxxxxxx',
		'MANAGERS__C' => '005Dxxxxxxxxxxxxxx'
	};

	//1.Data integrity check  2. Data format check
    public static Map<String,String> Datacheck(Data_validtion_Obj obj,String value){
    	String filedName = obj.fieldName;
    	Map<String,String> rebackMap = new Map<String,String>();

    		if(obj.formatType == 1){
    			rebackMap = Data_Check_With_Comma(obj,value);
    		}
    		if(obj.formatType == 2){
    			rebackMap = Data_Check_With_Semicolon(obj,value);
    		}
    		if(obj.formatType == 3){
    			rebackMap = Data_Check_With_Comma_Semicolon(obj,value);
    		}
    		if(obj.formatType == 4){
    			rebackMap = Data_Check_With_Semicolon_Comma(obj,value);
    		}

    	return rebackMap;
    }

    // for comma & Id check 
    public static Map<String,String> Data_Check_With_Comma(Data_validtion_Obj obj,String value){
    	String fix='';
    	Boolean needFix = false;
    	String fieldName = obj.fieldName;
    	Map<String,String> resultMap = new Map<String,String>();
    	List<String> semiList = value.split(';');
    	List<String> divideList = value.split('[,;]');
    	for(String str:divideList){
    		if(string.isEmpty(str)){
    			resultMap.put(statusList.get(0),'There has a empty element,Please check the data!' );
    			return resultMap;
    		}

    		if(str.startsWith(' ')|| str.endsWith(' ')){
    			needFix = true;
    		}
    	}

    	// check the ID length and prefix if necessary
    	if(obj.hasId){
    		resultMap = checkIdFormat(fieldName,divideList,false);
    		if(resultMap.size()>0){
    			return resultMap;
    		}
    	}
    	//check error：Mistaken input ',' to ';' and endwith';'or ','
    	if(semiList.size()>1 || value.endswith(';') || value.endsWith(',')){
    		needFix=true;
    	}
    	//fix data
    	if(needFix){
    		for(Integer i=0;i<divideList.size();i++){
    			if(i == divideList.size()-1){
    				fix+=divideList[i].trim();
    			}else{
    				fix+=divideList[i].trim()+',';
    			}
    			
    		}
    	}

    	if(String.isNotEmpty(fix)){
    		resultMap.put(statusList.get(1), fix);
    		return resultMap;
    	}

    	resultMap.put(statusList.get(2), value);
    	return resultMap;
    }

    // for Semicolon ; NO Id Check
    public static Map<String,String> Data_Check_With_Semicolon(Data_validtion_Obj obj,String value){
    	String fix='';
    	Boolean needFix = false;
    	Map<String,String> resultMap = new Map<String,String>();
    	List<String> commaList = value.split(',');
    	List<String> divideList = value.split('[,;]');
    	for(String str:divideList){
    		if(string.isEmpty(str)){
    			resultMap.put(statusList.get(0),'There has a empty element,Please check the data!' );
    			return resultMap;
    		}

    		if(str.startsWith(' ')|| str.endsWith(' ')){
    			needFix = true;
    		}

    	}
    	//check error：Mistaken input ';' to ',' and endwith';'or ','
    	if(commaList.size()>1 ||value.endswith(';')|| value.endsWith(',')){
    		needFix = true;
    	}
    	

    	if(needFix){
    		for(Integer i=0;i<divideList.size();i++){
    			if(i == divideList.size()-1){
    				fix+=divideList[i].trim();
    			}else{
    				fix+=divideList[i].trim()+',';
    			}
    		}
    	}
    	if(String.isNotEmpty(fix)){
    		resultMap.put(statusList.get(1), fix);
    		return resultMap;
    	}
    	resultMap.put(statusList.get(2), value);
    	return resultMap;
    }	

    // for Comma-Semicolon  ,count for the future, e.g: 3 values for a pair
    public static Map<String,String> Data_Check_With_Comma_Semicolon(Data_validtion_Obj obj,String value){
    	Integer count = obj.subListSize;
    	Integer index = obj.IdIndex;
    	String fieldName = obj.fieldName;
    	Map<String,String> resultMap = new Map<String,String>();
    	List<String> semiList = value.split(';');
    	List<String> singleList = value.split('[,;]');
    	//system.debug('singleList='+singleList);
   		//check the values: name,Id;name,Id
   		List<String> idList = new List<String>();
   		List<String> nameList = new List<String>();

   		for(Integer i=1;i<=singleList.size();i++){
   			if(math.mod(i-index,count)==0){
   				idList.add(singleList[i-1]);
   			}else{
   				nameList.add(singleList[i-1]);
   			}
   		}
   		system.debug('idlist='+idlist+'--nameList:'+nameList);
   		if(obj.hasId){
   			resultMap = checkIdFormat(fieldName,nameList,true);
   			system.debug('resultMap:'+resultMap);	
    		if(resultMap.size()>0){
    			return resultMap;
    		}else{
    			resultMap = checkIdFormat(fieldName,idList,false);
    			if(resultMap.size()>0){
    				return resultMap;
    			}
    		}
   		}
   		if(math.mod(singleList.size(),count)<>0){
   			resultMap.put(statusList.get(0),'Please check the end of the Data you Input!');
   			return resultMap;
   		}
   		
   		for(String valList: semiList){
   			List<String> commaList = valList.split(',');
   			system.debug('commaList.size():'+commaList.size());
   			if(math.mod(commaList.size(),count)<>0){
   				resultMap.put(statusList.get(0),'Please check the data format,it should be a comma or semicolon after【 '+commaList[commaList.size()-1]+' 】!');
   				return resultMap;
   			}
   			
   		}	
   		resultMap.put(statusList.get(2),value);
    	return resultMap;
    }

    // for Semicolon-Comma
    public static Map<String,String> Data_Check_With_Semicolon_Comma(Data_validtion_Obj obj,String value){
    	Integer count = obj.subListSize;
    	Integer index = obj.IdIndex;
    	String fieldName = obj.fieldName;
    	Map<String,String> resultMap = new Map<String,String>();
    	List<String> commaList = value.split(',');
    	List<String> singleList = value.split('[,;]');
    	List<String> idList = new List<String>();
   		List<String> nameList = new List<String>();

   		for(Integer i=1;i<=singleList.size();i++){
   			if(math.mod(i-index,count)==0){
   				idList.add(singleList[i-1]);
   			}else{
   				nameList.add(singleList[i-1]);
   			}
   		}
   		//system.debug('idlist='+idlist+'--nameList:'+nameList);
   		if(obj.hasId){
   			resultMap = checkIdFormat(fieldName,nameList,true);
   			system.debug('resultMap:'+resultMap);	
    		if(resultMap.size()>0){
    			return resultMap;
    		}else{
    			resultMap = checkIdFormat(fieldName,idList,false);
    			if(resultMap.size()>0){
    				return resultMap;
    			}
    		}
   		}
   		if(math.mod(singleList.size(),count)<>0){
   			resultMap.put(statusList.get(0),'Please check the end of the Data you Input!');
   			return resultMap;
   		}
   		
   		for(String valList: commaList){
   			List<String> semiList = valList.split(';');
   			system.debug('semiList.size():'+semiList.size());
   			if(math.mod(semiList.size(),count)<>0){
   				resultMap.put(statusList.get(0),'Please check the data format,it should be a comma or semicolon after【 '+commaList[commaList.size()-1]+' 】!');
   				return resultMap;
   			}
   			
   		}	
   		resultMap.put(statusList.get(2),value);
    	return resultMap;

    }


    // check ID/Name
    public static Map<String,String> checkIdFormat(String fieldName,List<String> idList,Boolean isNameCheck){
    	String demoStr = dataIDCheckMap.get(fieldName);
    	Map<String,String> returnMap = new Map<String,String>();
    	for(String val: idList){
    		system.debug('val.length='+val.length()+'--demo.lenth='+demoStr.length()+'val.prefix='+val.substring(0,4)+'demo.prefix='+demoStr.substring(0,4));
    		system.debug(!(val.length() == demoStr.length() && val.substring(0,4) == demoStr.subString(0,4)) || isNameCheck);
    		if(!isNameCheck){
    			if(!(val.length() == demoStr.length() && val.substring(0,4) == demoStr.subString(0,4))){
    				returnMap.put(statusList.get(0), 'The Id of the Data You Input was WRONG! Please Check the ID value!'+ '【 Field='+fieldName+' ID='+val+' 】 OR the mark around the value!');
					return returnMap;
				}
    		}else{
    			if((val.length() == demoStr.length() && val.substring(0,4) == demoStr.subString(0,4))){
    				returnMap.put(statusList.get(0), 'The Name of the Data You Input was WRONG! Please Check the Name value!'+ '【 Field='+fieldName+' Name='+val+' 】 OR the mark around the value!');
    				return returnMap;
    			}
    		}
    		
    	}
    	return returnMap;
    }


    public class Data_validtion_Obj{
    	/**
    	** formatType:
	    * '1' stand for ','
	    * '2' stand for ';'
	    * '3' stand for ',&;'
	    * '4' stand for ';&,'
    	*
    	*
    	*/
    	public string fieldName{get;set;}
    	public Integer formatType{get;set;}
    	public Boolean hasId{get;set;}
    	public Integer subListSize {get;set;}
    	public Integer IdIndex{get;set;}
    	public string IdFormat{get;set;}
    }

}