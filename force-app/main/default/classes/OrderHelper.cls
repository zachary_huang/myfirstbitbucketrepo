public without sharing class OrderHelper {
    /**
     * @name AfterUpdate
     * @description 
     * @param List<Order> newList
     * @param List<Order> oldList
     * @return void
    **/
    public static void AfterUpdate(List<Order> newList, List<Order> oldList){
        Set<Id> orderIds = new Set<Id>();
        for ( Integer i=0; i<newList.size(); i++ ){
            if ( newList[i].Status == Constants.ACTIVATED_ORDER_STATUS && oldList[i].Status != Constants.ACTIVATED_ORDER_STATUS ){
                orderIds.add(newList[i].Id);
            }
        }
        RollUpOrderItems(orderIds);
    }

    /**
     * @name RollUpOrderItems
     * @description Given a set of Activated Order ids, query the child Order Items and related Products to calculate Inventory levels
     * @param Set<Id> activatedOrderIds
     * @return void
    **/
    public static void RollUpOrderItems(Set<Id> activatedOrderIds){
        //ToDo: Declare a Map named "productMap" of Ids to Product2 records
        Map<Id, Product2> productMap;
        Set<Id> productSet = new Set<Id>();
        List<OrderItem> orderItems = [SELECT Id, Quantity,OrderId ,Product2Id FROM OrderItem WHERE OrderId IN:activatedOrderIds];
        //ToDo: Loop through a query of OrderItems related to the activatedOrderIds
        for(OrderItem oitem : orderItems){
            productSet.add(oitem.Product2Id);
        }
        //ToDo: Populate the map with the Id of the related Product2 as the key and Product2 record as the value
        productMap = new Map<Id,Product2>([SELECT Id, Quantity_Ordered__c  FROM Product2 WHERE Id IN: productSet]);

        //ToDo: Loop through a query that aggregates the OrderItems related to the Products in the ProductMap keyset
        AggregateResult[] groupedResult  = [SELECT Product2Id, SUM(Quantity) totalQuantity FROM OrderItem WHERE orderId IN:activatedOrderIds AND Product2Id IN: productSet GROUP BY Product2Id ];

        for(AggregateResult ar: groupedResult){
            if(productMap.containskey(String.valueOf(ar.get('Product2Id')))){
                productMap.get(String.valueOf(ar.get('Product2Id'))).Quantity_Ordered__c = Integer.valueOf(ar.get('totalQuantity'));
            }
        }
        //ToDo: Perform an update on the records in the productMap
        update productMap.values();
    }

}