global class WarehouseCalloutServiceMock implements HttpCalloutMock{
    // implement http mock callout
    global HTTPResponse respond(HTTPRequest req) {
    	String res = '[{"_id":"55d66226726b611100aaf741","replacement":false,"quantity":5,"name":"Generator 1000 kW","maintenanceperiod":365,"lifespan":120,"cost":5000,"sku":"100003"},{"_id":"55d66226726b611100aaf741","replacement":false,"quantity":5,"name":"Generator 1000 kW","maintenanceperiod":365,"lifespan":120,"cost":5000,"sku":"100004"}]';
    	// System.assertEquals('https://www.baidu.com', req.getEndpoint());
    	System.assertEquals('GET', req.getMethod());

    	HttpResponse respond = new HTTPResponse();
    	respond.setBody(res);
    	respond.setStatusCode(200);
    	
    	return respond;
    }

}