/*******************************************************************************
 *  Summary         : 
 *  Refer Object    : 
 *  Author          : Zachary Huang	      
 *  CreatedDate     : 2020-07-24
 *  Change Log      : 
 ******************************************************************************/

public with sharing class BoatSearchResults {

	@AuraEnabled
	public static List<BoatType__c> getBoatTypes(){
		return [SELECT Id,Name FROM BoatType__c];
	}



    @AuraEnabled
    public static List<Boat__c> getBoats(String boatTypeId) {
    	String querySql = 'SELECT Id,Name,Picture__c,Geolocation__Latitude__s,Geolocation__Longitude__s FROM Boat__c ';
    	if(String.isNotBlank(boatTypeId)){
    		querySql += ' WHERE ' + ' BoatType__c =: boatTypeId'  ;
    	}
        
        System.debug(LoggingLevel.INFO, '*** querySql: ' + querySql);
        return Database.query(querySql);
    }
}