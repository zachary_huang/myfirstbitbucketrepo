public class SFUtilty {
    @future
    public static void setUrl(Map<String,String> relationMap){
        Map<Id,Contact> contactMap = new Map<Id,Contact>([Select id,sfUrl__c from contact where id in:relationMap.values()]);   
            Map<ID,ContentDistribution> conDisMap = new Map<Id,ContentDistribution>([Select id,DistributionPublicUrl,ContentVersionId from ContentDistribution where ContentVersionId in:relationMap.keySet() ]);
            system.debug('contentdistribution:'+conDisMap);
            for(ID contactId: contactMap.keySet() ){
                if(relationMap.containsKey(contactId) && conDisMap.containsKey(relationMap.get(contactId))){
                    contactMap.get(contactId).sfUrl__c = conDisMap.get(relationMap.get(contactId)).DistributionPublicUrl;
				}
			}
            system.debug('contactMap:'+contactMap.values());
            update contactMap.values();
	} 
}