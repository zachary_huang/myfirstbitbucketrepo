public with sharing class campaignMemberTransfer {
    @AuraEnabled
    public static List<Campaign> SearchCampaign(String camName) {
        camName = '%' + camName + '%';
        return [SELECT Id, Status, StartDate, Name, EndDate From Campaign WHERE Name like: camName];
    }



    @AuraEnabled
    public static Boolean TransferCampaignMember(String oldCamId, String newCamId) {
    	
    	List<CampaignMember> cmlst = new List<CampaignMember>();
    	try{
    		for(CampaignMember cm : [SELECT Id,CampaignID FROM CampaignMember WHERE CampaignID=:oldCamId]){
	        	cm.CampaignID = newCamId;
	        	cmlst.add(cm);
	        }

	        update cmlst;
	        return true;

    	}catch(Exception e){
    		System.debug(LoggingLevel.INFO, '*** error Message : ' + e.getStackTraceString() + e.getMessage());
    		return false;
    	}
        
    }

}